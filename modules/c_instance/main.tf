resource "google_compute_instance" "vm-instance" {
  name                     = "lamp-tutorial"
  metadata_startup_script = file("../../startup.sh")
  machine_type         = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = google_compute_network.vpc-network.name
    # Assign public IP to the machine
    access_config {
    }
  }

  scheduling {
    automatic_restart = false
    preemptible       = true
    }
}