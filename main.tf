terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file(var.gcp_key)

  project = file(var.project_id)
  region  = "us-central1"
  zone    = "us-central1-c"
}

# Create a VPC network
resource "google_compute_network" "vpc_network" {
  name                    = "lamp-network"
  auto_create_subnetworks = false
}

#Create Subnet
resource "google_compute_subnetwork" "subnet-prod" {
  name          = "lamp-prod"
  ip_cidr_range = "192.168.16.0/20"
  region = "us-central1"
  network       = google_compute_network.vpc_network.name
}

#Create ah HTTP/S firewall rule
resource "google_compute_firewall" "rule-http" {
  name      = "lamp-rule-http"
  network   = google_compute_network.vpc_network.name
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
}

#Create an SSH firewall  to access machine from GCS only
resource "google_compute_firewall" "rule-ssh" {
  name          = "lamp-rule-ssh"
  description   = "Allow SSH connections from inside GCS"
  network       = google_compute_network.vpc_network.name
  direction     = "INGRESS"
  source_ranges = ["35.235.240.0/20"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}
# Create manager compute instance

resource "google_compute_instance" "vm-instance" {
  name = "lamp-tutorial"
  #  metadata_startup_script = file("../../startup.sh")
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet-prod.name
    #   Assign ephemeral public IP to the machine
    /*  
    access_config {
    }
*/
  }

  scheduling {
    automatic_restart = false
    preemptible       = true
  }
}


#Create a compute instance from module
/*
 module "c_instances" {
  source = "./modules/c_instance/"
  def_network = google_compute_network.vpc_network
  depends_on = [
    google_compute_network.vpc_network
  ]
}
*/

#Create service account for k8s cluster
resource "google_service_account" "k8s_account" {
  account_id = "k8s-service-account"
  display_name = "K8s Service account"
}

#Create k8s cluster
resource "google_container_cluster" "cluster" {
  name = "lamp-cluster"
  location = "us-central1-c"
  initial_node_count = 2
  node_config {
    service_account = google_service_account.k8s_account.email
    
  }
  network = google_compute_network.vpc_network.name
  subnetwork = google_compute_subnetwork.subnet-prod.name
}

#Create node pool for k8s cluster
resource "google_container_node_pool" "node_pool" {
  name = "k8s-node-pool"
  location = "us-central1-c"
  cluster = google_container_cluster.cluster.name
  node_count = 2
  
  node_config {
    preemptible = true
    machine_type = "n1-standard-1"
    disk_type = "pd-standard"
    disk_size_gb = 10
  }
}

#Output var for manager CI
/*
output "IP" {
  value = google_compute_instance.vm-instance.network_interface[0].access_config[0].nat_ip
}
*/