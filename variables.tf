variable "gcp_key" {
  description = "Location of the json key for GCP service account"
  type        = string
  default     = "./secrets/terraform-key.json"
  sensitive   = true
}

variable "project_id" {
  description = "GCP Project ID"
  type        = string
  default     = "./secrets/project-id"
  sensitive   = true
}

/*
variable "def_network" {
  description = "VPC network"
  type = string
}
*/